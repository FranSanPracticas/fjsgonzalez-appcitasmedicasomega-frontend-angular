import { TestBed } from '@angular/core/testing';

import { CitaGuardService } from './cita-guard.service';

describe('CitaGuardService', () => {
  let service: CitaGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CitaGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

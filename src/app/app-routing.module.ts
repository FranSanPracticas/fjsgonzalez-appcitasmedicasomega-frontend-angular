import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegistroComponent } from './auth/registro/registro.component';
import { DetalleCitaComponent } from './cita/detalle-cita/detalle-cita.component';
import { EditarCitaComponent } from './cita/editar-cita/editar-cita.component';
import { ListaCitaComponent } from './cita/lista-cita/lista-cita.component';
import { NuevaCitaComponent } from './cita/nueva-cita/nueva-cita.component';
import { CitaGuardService as guard } from './guards/cita-guard.service';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'login', component:LoginComponent},
  {path: 'registro', component:RegistroComponent},
  {path: 'lista', component:ListaCitaComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user']}},
  {path: 'detalle/:id', component: DetalleCitaComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user']}},
  {path: 'nueva', component: NuevaCitaComponent, canActivate: [guard], data: { expectedRol: ['admin']}},
  {path: 'editar/:id', component: EditarCitaComponent, canActivate: [guard], data: { expectedRol: ['admin']}},
  {path: '**', redirectTo: '', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

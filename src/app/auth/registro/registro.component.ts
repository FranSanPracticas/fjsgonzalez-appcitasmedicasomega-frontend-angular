import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NuevoUser } from 'src/app/models/nuevo-user';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  isLogged = false;

  isRegister = false;
  isRegisterFail = false;
  nuevoUser: NuevoUser;
  nombre: string;
  apellidos: string;
  usuario: string;
  clave: string;

  errMsj:string;

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private toastr: ToastrService, 
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.tokenService.getToken()){
      this.isLogged = true;
    }
  }

  onRegister(): void {
    this.nuevoUser = new NuevoUser(this.nombre, this.apellidos, this.usuario, this.clave);
    this.authService.nuevo(this.nuevoUser).subscribe(
      data => {
        this.toastr.success('Cuenta Creada', 'OK', {
          timeOut: 3000
        });
        
        this.isRegister = true;
        this.isRegisterFail = false;

        this.router.navigate(['/login']);
      },
      err => {
        this.isRegister = false;
        this.isRegisterFail = true;
        this.errMsj = err.error.mensaje;
        this.toastr.error(this.errMsj, 'Fail', {
          timeOut: 3000
        });
      }
    );
  }

}

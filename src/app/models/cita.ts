import { Diagnostico } from "./diagnostico";
import { Medico } from "./medico";
import { Paciente } from "./paciente";

export class Cita {

    id/*?*/: number;
    motivoCita: string;
    fechaHora: Date;
    paciente: Paciente;
    medico: Medico;
    diagnostico: Diagnostico;

    idPaciente:number;
    idMedico:number;
    idDiagnostico:number;

    constructor(id:number, motivoCita: string, fechaHora: Date, paciente: Paciente, medico: Medico, diagnostico: Diagnostico){
        this.id = id;
        this.motivoCita = motivoCita;
        this.fechaHora = fechaHora;
        this.paciente = paciente;
        this.medico = medico;
        this.diagnostico = diagnostico;
    }

    public set setIdPaciente(idPaciente:number){
        this.idPaciente = idPaciente;
    }

    public set setIdMedico(idMedico:number){
        this.idMedico = idMedico;
    }

    public set setIdDiagnostico(idDiagnostico:number){
        this.idDiagnostico = idDiagnostico;
    }

}

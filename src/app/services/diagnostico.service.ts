import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Diagnostico } from '../models/diagnostico';

@Injectable({
  providedIn: 'root'
})
export class DiagnosticoService {

  diagnosticosURL = 'http://localhost:8080/diagnosticos/';

  constructor(private httpClient: HttpClient) { }

  public getDiagnosticoById(id:number): Observable<Diagnostico>{
    return this.httpClient.get<Diagnostico>(this.diagnosticosURL + `${id}`);
  }

  public getVoidDiagnostico(): Observable<Diagnostico>{
    return of(new Diagnostico('', ''));
  }

}

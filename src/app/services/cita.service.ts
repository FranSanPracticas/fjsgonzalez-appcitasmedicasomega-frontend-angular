import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cita } from '../models/cita';

@Injectable({
  providedIn: 'root'
})
export class CitaService {

  citasURL = 'http://localhost:8080/citas/';

  constructor(private httpClient: HttpClient) { }

  public getCitas(): Observable<Cita[]> {
    return this.httpClient.get<Cita[]>(this.citasURL);
  }

  public getCitaById(id:number): Observable<Cita> {
    return this.httpClient.get<Cita>(this.citasURL + `${id}`);
  }

  public addCita(cita:Cita): Observable<any>{
    return this.httpClient.post<any>(this.citasURL + 'add', cita);
  }

  /* public updateCita(id:number, cita:Cita): Observable<any>{
    return this.httpClient.put<any>(this.citasURL + `update/${id}`, cita);
  } */

  public updateCita(cita:Cita): Observable<any>{
    return this.httpClient.put<any>(this.citasURL + `update`, cita);
  }

  public deleteCita(id:number):Observable<any>{
    return this.httpClient.delete<any>(this.citasURL + `delete/${id}`);
  }


}

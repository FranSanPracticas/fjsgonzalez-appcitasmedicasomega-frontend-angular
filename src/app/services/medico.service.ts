import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medico } from '../models/medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  medicosURL = 'http://localhost:8080/medicos/';

  constructor(private httpClient: HttpClient) { }

  public getMedicoById(id:number): Observable<Medico>{
    return this.httpClient.get<Medico>(this.medicosURL + `${id}`);
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from '../models/paciente';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  pacientesURL = 'http://localhost:8080/pacientes/';

  constructor(private httpClient: HttpClient) { }

  public getPacienteById(id:number): Observable<Paciente>{
    return this.httpClient.get<Paciente>(this.pacientesURL + `${id}`);
  }

}

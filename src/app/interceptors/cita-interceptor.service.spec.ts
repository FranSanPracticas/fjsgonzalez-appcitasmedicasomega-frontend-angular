import { TestBed } from '@angular/core/testing';

import { CitaInterceptorService } from './cita-interceptor.service';

describe('CitaInterceptorService', () => {
  let service: CitaInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CitaInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

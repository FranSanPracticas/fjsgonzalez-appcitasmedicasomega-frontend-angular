import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Cita } from 'src/app/models/cita';
import { CitaService } from 'src/app/services/cita.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-lista-cita',
  templateUrl: './lista-cita.component.html',
  styleUrls: ['./lista-cita.component.css']
})
export class ListaCitaComponent implements OnInit {

  citas: Cita[] = [];
  roles: string[];
  isAdmin = false;

  constructor(private citaService: CitaService, private toastr:ToastrService, private tokenService: TokenService) { }

  ngOnInit(): void {
    this.cargarListaCitas();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if(rol == 'ROLE_ADMIN'){
        this.isAdmin = true;
      }
    });
  }

  cargarListaCitas(): void {
    this.citaService.getCitas().subscribe(
      data => {
        this.citas = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  borrar(id:number){
    this.citaService.deleteCita(id).subscribe(
      data => {
        this.toastr.success('Cita Eliminada', 'OK', {
          timeOut: 3000
        });
        this.cargarListaCitas();
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
      }
    );
  }

}

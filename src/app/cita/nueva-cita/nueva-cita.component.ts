import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cita } from 'src/app/models/cita';
import { Diagnostico } from 'src/app/models/diagnostico';
import { Medico } from 'src/app/models/medico';
import { Paciente } from 'src/app/models/paciente';
import { CitaService } from 'src/app/services/cita.service';
import { DiagnosticoService } from 'src/app/services/diagnostico.service';
import { MedicoService } from 'src/app/services/medico.service';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-nueva-cita',
  templateUrl: './nueva-cita.component.html',
  styleUrls: ['./nueva-cita.component.css']
})
export class NuevaCitaComponent implements OnInit {

  motivoCita: string = '';
  id: number = 0;
  fechaHora: Date = new Date();//new Date("1995-12-17T03:24:00");
  idPaciente: number;
  paciente: Paciente; //= new Paciente();
  idMedico: number;
  medico: Medico;
  idDiagnostico: number;
  diagnostico: Diagnostico;

  cita = {} as Cita;


  constructor(private citaService: CitaService, 
    private pacienteService: PacienteService,
    private medicoService: MedicoService,
    private diagnosticoService: DiagnosticoService,
    private toastr:ToastrService, 
    private router:Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  onCreate(): void{

    this.pacienteService.getPacienteById(this.idPaciente).subscribe(
      data =>{
        this.cita.paciente = data;
        
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
      }
    );

    console.log("NombrePaciente=" + this.cita.paciente.nombre);

    this.medicoService.getMedicoById(this.idMedico).subscribe(
      data =>{
        this.cita.medico = data;
        
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
      }
    );

    console.log("NombreMedico=" + this.cita.medico.nombre);


    this.diagnosticoService.getDiagnosticoById(this.idDiagnostico).subscribe(
      data =>{
        this.cita.diagnostico = data;
        
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
      }
    );
    

    console.log("Diagnostico=" + this.cita.diagnostico.enfermedad);
    

    //const cita = new Cita(this.id, this.motivoCita, this.fechaHora, this.paciente, this.medico, this.diagnostico);
    

    this.citaService.addCita(this.cita).subscribe(
      data => {
        this.toastr.success('Cita Creada', 'OK', {
          timeOut: 3000
        });
        this.router.navigate(['/lista']);


      }, 
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
        //this.router.navigate(['/lista']);
      }

    );
  }
  

}

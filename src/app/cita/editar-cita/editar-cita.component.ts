import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cita } from 'src/app/models/cita';
import { CitaService } from 'src/app/services/cita.service';
import { DiagnosticoService } from 'src/app/services/diagnostico.service';
import { MedicoService } from 'src/app/services/medico.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { Diagnostico } from 'src/app/models/diagnostico';
import { Medico } from 'src/app/models/medico';
import { Paciente } from 'src/app/models/paciente';


@Component({
  selector: 'app-editar-cita',
  templateUrl: './editar-cita.component.html',
  styleUrls: ['./editar-cita.component.css']
})
export class EditarCitaComponent implements OnInit {

  cita = {} as Cita;

  motivoCita: string = '';
  id: number = 0;
  fechaHora: Date = new Date();
  idPaciente: number;
  paciente: Paciente; //= new Paciente();
  idMedico: number;
  medico: Medico;
  idDiagnostico: number;
  diagnostico: Diagnostico;


  constructor(private citaService:CitaService, 
    private pacienteService: PacienteService,
    private medicoService: MedicoService,
    private diagnosticoService: DiagnosticoService,
    private activatedRoute: ActivatedRoute, 
    private toastr: ToastrService, 
    private router: Router) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    this.citaService.getCitaById(id).subscribe(
      data =>{
        this.cita = data;
        this.idPaciente = this.cita.paciente.id;
        this.idMedico = this.cita.medico.id;
        this.idDiagnostico = this.cita.diagnostico.id;
        console.log("fechaHora" + this.cita.fechaHora);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
      }
    );
  }

  onUpdate(): void{

    
    if(this.cita.paciente.id == this.idPaciente){
      this.paciente = this.cita.paciente;
    } else {
      this.pacienteService.getPacienteById(this.idPaciente).subscribe(
        data =>{
          this.paciente = data;
          
        },
        err => {
          this.toastr.error(err.error.mensaje, 'Fail', {
            timeOut: 3000
          });
        }
      );
  
      console.log("NombrePaciente=" + this.paciente.nombre);
    }


    if(this.cita.medico.id == this.idMedico){
      this.medico = this.cita.medico;
    } else {
      this.medicoService.getMedicoById(this.idMedico).subscribe(
        data =>{
          this.medico = data;
          
        },
        err => {
          this.toastr.error(err.error.mensaje, 'Fail', {
            timeOut: 3000
          });
        }
      );
  
      console.log("NombreMedico=" + this.medico.nombre);
    }


    if(this.cita.diagnostico.id == this.idDiagnostico){
      this.diagnostico = this.cita.diagnostico;
    } else {
      this.diagnosticoService.getDiagnosticoById(this.idDiagnostico).subscribe(
        data =>{
          this.diagnostico = data;
          
        },
        err => {
          this.toastr.error(err.error.mensaje, 'Fail', {
            timeOut: 3000
          });
        }
      );
      
  
      console.log("Diagnostico=" + this.diagnostico.enfermedad);   
    }
    


    const citaUpdated = new Cita(this.cita.id, this.cita.motivoCita, this.cita.fechaHora, this.paciente, this.medico, this.diagnostico);

    this.citaService.updateCita(citaUpdated).subscribe(
      data => {
        this.toastr.success('Cita Actualizada', 'OK', {
          timeOut: 3000
        });
        this.router.navigate(['/lista']);


      }, 
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000
        });
        //this.router.navigate(['/lista']);
      }
    );






  }

}
